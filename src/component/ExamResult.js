import React, { Component } from 'react';
import {connect} from 'react-redux';
 class ExamResult_ extends Component {
  render() {
    let Subjects  = this.props.ExamReducer.userData.Subjects;
    let SubjectsNames = [];
    let SubjectGrades = [];
    Subjects.map((subject,index) => {
      SubjectsNames.push(<th key={index}>{subject.SubjectName}</th>);
      SubjectGrades.push(<td key={index}>{subject.SubjectGrade}</td>)
    })
    return (
      <div>
        <table>
          <tr>
            {SubjectsNames}
          </tr>
          <tr>
            {SubjectGrades}
          </tr>
        </table>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    ...state
  }
}
export const  ExamResult = connect(mapStateToProps)(ExamResult_)
