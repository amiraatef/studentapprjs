import React, { Component } from 'react'
import AppBar from 'material-ui/AppBar';
import { connect } from 'react-redux'
import { UserProfile, ExamResult } from './index'
import { NavLink } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import Footer from "react-footer-comp"
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class HomeScreen extends Component {
  render() {
    let Userprofile, Examsresult = null
    if (this.props.ExamReducer.UserprofileClicked) {
      Userprofile = <UserProfile />
      Examsresult = null

    }
    if (this.props.ExamReducer.ExamsResultclicked) {
      Examsresult = <ExamResult />
      Userprofile = null
    }
    return (

      <div>
        <MuiThemeProvider>

          <AppBar 
            title="Home"
          />
          <div class='divcmp'>
            {Userprofile}
            {Examsresult}
          </div>
          <div  class='btns' >
            <RaisedButton label="UserProfile" primary={true} onClick={()=>this.props.setProfileOn()} />
            <br />
            <br/>
            <RaisedButton label="ExamResult" primary={true} onClick={()=>this.props.setExamResultOn()} />
          </div>
        </MuiThemeProvider>

      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    setProfileOn: () => dispatcher({ type: 'profileOn' }),
    setExamResultOn: () => dispatcher({ type: 'examResultOn' }),

  }
}
export const Home = connect(mapStateToProps, mapDispatchersToProps)(HomeScreen);