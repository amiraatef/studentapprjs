import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {NavLink} from 'react-router-dom'
import  './Styles.css'
import axios from 'axios';
import { connect } from 'react-redux'

export class LoginScreen extends Component {
  constructor(props) {
    super(props)  
    this.state = {
      Email:'',
      password:'',
    };
  };
 LoginUser = (LoginUserData) => {
console.log("LoginUserData",LoginUserData)
  return new Promise((resolve, reject) => {

    axios.get('https://examsapp-47879.firebaseio.com/students.json')
      .then((response) => {


        if (response.data) {
          let responseKeys = Object.keys(response.data);

          for (let i = 0; i < responseKeys.length; i++) {

            if (response.data[responseKeys[i]].Email === LoginUserData.Email) {
console.log( LoginUserData.Email,response.data[responseKeys[i]].Email)
              if (response.data[responseKeys[i]].password === LoginUserData.password) {
                console.log( LoginUserData.Email,response.data[responseKeys[i]].Email)

                resolve({
                  LoginState: 'Valid',
                  UserData: response.data[responseKeys[i]]
                });

              }
            }
          }
          resolve({
            LoginState: 'Not Valid'
          });
          return;
        }
      }).catch((error) => reject(error));




  })


}

login=()=> {
  const Newuser = {
    Email: this.state.Email,
    password: this.state.password,
  }
  this.LoginUser(Newuser).then((response) => {

    if (response.LoginState === 'Valid') {
     this.props.setUserData(response.UserData);
    }
    else {
      alert('Email or/and Password is incorrect');
    }
  })
}
render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title="Login"
           />
           <div class="login">
           <TextField
             hintText="Enter your Email"
                   onChange = {(event,newValue) => this.setState({Email:newValue})}
             />
           <br/>
             <TextField
               type="password"
               hintText="Enter your Password"
               onChange = {(event,newValue) => this.setState({password:newValue})}
               />
             <br/>
             <RaisedButton primary={true} onClick={this.login} >
                  <NavLink to='/Home'>Login</NavLink>
                  </RaisedButton>
                  <RaisedButton primary={true} >

             <NavLink to='/SignUp' >SignUp</NavLink>
                               </RaisedButton>


         </div>
         </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    setUserData: (UserData) => dispatcher({ type: 'filuserData' ,value: UserData}),

  }
}
export const Login = connect(mapStateToProps, mapDispatchersToProps)(LoginScreen);