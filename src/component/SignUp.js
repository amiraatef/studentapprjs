import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {NavLink} from 'react-router-dom'
import axios from 'axios';

export  class SignUp extends Component {
constructor(props) {
  super(props)

  this.state = {
    username:'',
    Email:'',
    password:'',
  
  };
};
SignUpNewUser = (UserData) => {
  axios.get('https://examsapp-47879.firebaseio.com/students.json')
    .then((response) => {
      if (response.data) {
        let responseKeys = Object.keys(response.data);
        for (let i = 0; i < responseKeys.length; i++) {
          if (response.data[responseKeys[i]].Email === UserData.Email) {
            return 'Email exists before';
          }
        }
      }
      axios.post('https://examsapp-47879.firebaseio.com/students.json', UserData)
        .then((response) => {
          console.log(response)
        }).catch((error) => console.log(error));

    })
    .catch((error) => console.log(error));
}
signup=()=>
{

  const Newuser = {
    Email:this.state.Email,
    Password: this.state.password,
    UserName: this.state.username,
    Subjects:[
      {SubjectName: 'Arabic' , SubjectGrade: 5},
      {SubjectName: 'English' , SubjectGrade: 15},
      {SubjectName: 'Math' , SubjectGrade: 12},
  ]
  }
console.log("Newuser",Newuser)
  this.SignUpNewUser(Newuser);

}

  render() {
    return (
      <div>
      <MuiThemeProvider>
        <div>
        <AppBar
           title="SginUp"
         />
         <div >
         <TextField
           hintText="Enter your Username"
                 onChange = {(event,newValue) => this.setState({username:newValue})}
           />
         <br/>
         <TextField type='email'
           hintText="Enter your Email "
                 onChange = {(event,newValue) => this.setState({Email:newValue})}
           />
         <br/>
           <TextField
             type="password"
             hintText="Enter your Password"
             onChange = {(event,newValue) => this.setState({password:newValue})}
             />
           <br/>
           <RaisedButton primary={true} onClick={this.signup}>
                <NavLink to='/'>SignUp</NavLink>
                </RaisedButton>
       </div>
       </div>
       </MuiThemeProvider>
    </div>
    )
  }
}
