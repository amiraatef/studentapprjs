import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Route, Switch , BrowserRouter as Router } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'
import {Login ,SignUp,Home , ExamResult,UserProfile} from './index'
import {Provider} from 'react-redux';
import {store} from '../Reducers/store'
const history = createHistory()

export const Routes = (
  <Provider store={store}>
<Router history={history}>
          <Switch>
            <div>
            <Route exact path="/" component={Login}   />
            <Route path="/SignUp" component={SignUp} />
            <Route path="/Home" component={Home}    />
            <Route path="/ExamResult" component={ExamResult} />
            <Route path="/UserProfile" component={UserProfile}/>
            </div>
          </Switch>
        </Router>
</Provider>

    );