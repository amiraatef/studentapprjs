const initialState = {
    UserprofileClicked:true,
    ExamsResultclicked:false,
    userData:null
}
const reducer = (state = initialState, action) => {
    if (action.type == 'profileOn') {
        return {
            ...state,
            UserprofileClicked:true,
            ExamsResultclicked:false
        }
    }
    else if (action.type === 'examResultOn') {
        return {
            ...state,
            ExamsResultclicked:true,
            UserprofileClicked:false,
        }
    }
    
    else if (action.type === 'filuserData') {
        return {
            ...state,
           userData: action.value
        }
    }
        return state;
}

export default reducer;