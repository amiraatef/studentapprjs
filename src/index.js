import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {Routes} from './component/Routes'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
ReactDOM.render(Routes, document.getElementById('root'));
serviceWorker.unregister();
